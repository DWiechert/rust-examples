pub fn run() {
    // Print to console
    println!("Hello from the print.rs file.");

    // Basic formatting
    println!("Number: {}", 1);
    println!("{} variables are {}.", "Multiple", "injected");

    // Positional arguments
    println!("{0} an from {1} and {0} like to {2}.", "I", "Chicago", "code");

    // Named arguments
    println!("{name} like to play {activity}.", name= "I", activity = "football");

    // Placeholder traits
    println!("Binary: {:b}, Hex {:x}, Octal: {:o}", 10, 10, 10);

    // Placeholder for debug trait - printing a tuple
    println!("{:?}", (12, true, "hello."));

    // Basic math
    println!("10 + 10 = {}", 10 + 10);
}