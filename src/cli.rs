use std::env;

pub fn run() {
    println!("Hello from the cli.rs file.");

    let args: Vec<String> = env::args().collect();
    println!("Args: {:?}", args);

    // Zero index is binary being ran
    let command = args[1].clone();
    println!("Command: {}", command);

    let name = "Dan";
    let status = "100%";
    if command == "hello" {
        println!("Hi {}, how are you?", name);
    } else if command == "status" {
        println!("Status: {}", status);
    } else {
        println!("Invalid command.");
    }
}