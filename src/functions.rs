// Functions - Used to store blocks of code for re-use

pub fn run() {
    println!("Hello from the functions.rs file.");

    greeting("Hello", "Dan");

    // Bind function values to variables
    let get_sum = add(5, 5);
    println!("Sum: {}", get_sum);

    // Closure - inline functions (pipes indicate the parameters)
    let n3: i32 = 10;
    let add_nums = |n1: i32, n2: i32| n1 + n2 + n3;
    println!("Closure sum: {}", add_nums(3, 3));
}

fn greeting(greet: &str, name: &str) {
    println!("{} {}, nice to meet you.", greet, name);
}

fn add(n1: i32, n2: i32) -> i32 {
    // No semi-colon indicates this is the return value
    n1 + n2
}